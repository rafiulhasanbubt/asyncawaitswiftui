//
//  SpamWordsView.swift
//  AsyncAwaitSwiftUI
//
//  Created by rafiul hasan on 13/12/21.
//

import SwiftUI

struct LanguageListView: View {
    let service = SpamWordsLanguagesService()
    @State var languages: [Language] = []
    
    var body: some View {
        NavigationView {
            List(languages) { language in
                NavigationLink(destination:
                                SpamWordsView(language: language))
                {
                    Text(language.label)
                }
            }
            .navigationTitle("Languages")
        }
        .listStyle(.plain)
        .task {
            languages = await service.fetchLanguages()
        }
    }
}

struct LanguageListView_Previews: PreviewProvider {
    static var previews: some View {
        LanguageListView()
    }
}
