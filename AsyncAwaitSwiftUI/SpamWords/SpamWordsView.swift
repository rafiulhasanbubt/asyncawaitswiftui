//
//  SpamWordsView.swift
//  AsyncAwaitSwiftUI
//
//  Created by rafiul hasan on 13/12/21.
//

import SwiftUI

struct SpamWordsView: View {
    let language: Language
    let service = SpamWordsLanguagesService()
    @State var words: [String] = []
    
    var body: some View {
        List(words, id: \.self) { word in
            Text(word)
        }
        .navigationTitle(language.label)
        .task {
            words = await service.fetchWords(language: language)
        }
    }
}

//struct SpamWordView_Previews: PreviewProvider {
//    static var previews: some View {
//        SpamWordsView(language: Language())
//    }
//}
