//
//  SpamWordsLanguagesService.swift
//  AsyncAwaitSwiftUI
//
//  Created by rafiul hasan on 13/12/21.
//

import Foundation

struct SpamWordsLanguagesService {
    private let decoder: JSONDecoder = {
        let decoder = JSONDecoder()
        decoder.keyDecodingStrategy = .convertFromSnakeCase
        return decoder
    }()
    
    private func fetch<T: Decodable>(type: T.Type, from urlString: String) async -> T? {
        guard let url = URL(string: urlString) else {
            return nil
        }
        do {
            let (data, _) = try await URLSession
                .shared
                .data(from: url)
            return try decoder.decode(type, from: data)
        } catch {
            return nil
        }
    }
    
    func fetchLanguages() async -> [Language] {
        await fetch(type: LanguageList.self, from: "https://www.spam-words.com/api/languages")? .codeLanguages ?? []
    }
                
    func fetchWords(language: Language) async -> [String] {
        await fetch(type: SpamWords.self, from: "https://www.spam-words.com/api/words/" + language.code)?
            .words ?? []
    }
}
