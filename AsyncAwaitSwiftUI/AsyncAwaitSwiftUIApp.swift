//
//  AsyncAwaitSwiftUIApp.swift
//  AsyncAwaitSwiftUI
//
//  Created by rafiul hasan on 13/12/21.
//  E4sAKFg1QtDShzhJe9MNx2oTPAdOnfSG

import SwiftUI

@main
struct AsyncAwaitSwiftUIApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
