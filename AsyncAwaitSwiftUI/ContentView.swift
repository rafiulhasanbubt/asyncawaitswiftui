//
//  ContentView.swift
//  AsyncAwaitSwiftUI
//
//  Created by rafiul hasan on 13/12/21.
//

import SwiftUI

struct ContentView: View {
    var body: some View {
        TabView {
            IncrementView()
                .tabItem {
                    Label("Increament", systemImage: "person.fill")
                }
            LanguageListView()
                .tabItem {
                    Label("Word", systemImage: "pencil")
                }
            CryptoView()
                .tabItem {
                    Label("Crypto", systemImage: "square.and.pencil")
                }
            GifListView()
                .tabItem {
                    Label("Gif List", systemImage: "list.dash")
                }
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
