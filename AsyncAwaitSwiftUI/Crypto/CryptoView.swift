//
//  CryptoView.swift
//  AsyncAwaitSwiftUI
//
//  Created by rafiul hasan on 13/12/21.
//

import SwiftUI

struct CryptoView: View {
    let service = CryptoService()
    @State var coins: [Coin] = []
    
    var body: some View {
        List(coins) {
            CoinView(coin: $0)
        }
        .listStyle(.plain)
        .refreshable {
            coins = await service.fetchCoins()
        }
        .task {
            coins = await service.fetchCoins()
        }
    }
}

struct CryptoView_Previews: PreviewProvider {
    static var previews: some View {
        CryptoView()
    }
}
