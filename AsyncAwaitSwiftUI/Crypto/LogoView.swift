//
//  LogoView.swift
//  AsyncAwaitSwiftUI
//
//  Created by rafiul hasan on 13/12/21.
//

import SwiftUI

struct LogoView: View {
    let coin: Coin
    
    var body: some View {
        AsyncImage(
            url: URL(string: coin.logo),
            content: { image in
                image.resizable()
                    .aspectRatio(contentMode: .fit)
                    .frame(maxWidth: 40, maxHeight: 40)
            },
            placeholder: {
                ProgressView()
            }
        )
    }
}

struct LogoView_Previews: PreviewProvider {
    static var previews: some View {
        LogoView(coin: Coin(id: 123, coinName: "", acronym: "", logo: ""))
    }
}
