//
//  CoinView.swift
//  AsyncAwaitSwiftUI
//
//  Created by rafiul hasan on 13/12/21.
//

import SwiftUI

struct CoinView: View {
    let coin: Coin
    
    var body: some View {
        HStack {
            Text("\(coin.acronym): \(coin.coinName)")
            Spacer()
            LogoView(coin: coin)
        }
    }
}

struct CoinView_Previews: PreviewProvider {
    static var previews: some View {
        CoinView(coin: Coin(id: 23654, coinName: "", acronym: "", logo: ""))
    }
}
