//
//  Response.swift
//  AsyncAwaitSwiftUI
//
//  Created by rafiul hasan on 13/12/21.
//

import Foundation

struct Response: Codable {
    let data: [Gif]
}

struct Gif: Identifiable, Codable, Equatable {
    static func == (lhs: Gif, rhs: Gif) -> Bool {
        lhs.id == rhs.id
    }
    let id: String
    let title: String
    var url: String {
        images.downsized.url
    }
    let images: Images
}

struct Images: Codable {
    let downsized: Image
}

struct Image: Codable {
    let url: String
}
