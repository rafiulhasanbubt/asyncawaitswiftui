//
//  GifService.swift
//  AsyncAwaitSwiftUI
//
//  Created by rafiul hasan on 13/12/21.
// https://api.giphy.com/v1/gifs/search?api_key=E4sAKFg1QtDShzhJe9MNx2oTPAdOnfSG&q=cat&limit=10&offset=10

import Foundation

struct GifService {
    private let apiKey = "E4sAKFg1QtDShzhJe9MNx2oTPAdOnfSG"
    private let pageSize = 10
    private let query = "cat"
    
    private let decoder: JSONDecoder = {
        let decoder = JSONDecoder()
        decoder.keyDecodingStrategy =
            .convertFromSnakeCase
        return decoder
    }()
    
    func fetchGifs(page: Int) async -> [Gif] {
        let offset = page * pageSize
        guard let url = URL(string: "https://api.giphy.com/v1/gifs/search?api_key=\(apiKey)&q=\(query)&limit=\(pageSize)&offset=\(offset)") else { return [] }
        do {
            let (data, _) = try await URLSession.shared.data(from: url)
            print(data)
            let response = try decoder.decode(Response.self, from: data)
            return response.data
        } catch {
            print(error)
            return []
        }
    }
}
