//
//  GifListView.swift
//  AsyncAwaitSwiftUI
//
//  Created by rafiul hasan on 13/12/21.
//

import SwiftUI
import NukeUI

struct GifListView: View {
    let service = GifService()
    @State var gifs: [Gif] = []
    @State var page = 1
    
    var body: some View {
        List(gifs) { gif in
            VStack {
                LazyImage(source: gif.url)
                    .aspectRatio(contentMode: .fit)
                Text(gif.title)
            }.task {
                if gif == gifs.last {
                    page += 1
                    gifs += await service.fetchGifs(page: page)
                }
                gifs = await service.fetchGifs(page: page)
            }
        }.listStyle(.plain)
            
    }
}

struct GifListView_Previews: PreviewProvider {
    static var previews: some View {
        GifListView()
    }
}
