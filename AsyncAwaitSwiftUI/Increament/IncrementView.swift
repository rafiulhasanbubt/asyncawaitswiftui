//
//  IncrementView.swift
//  AsyncAwaitSwiftUI
//
//  Created by rafiul hasan on 13/12/21.
//

import SwiftUI

struct IncrementView: View {
    let service = Service()
    @State var value: String = ""
    @State var counter = 0
    
    var body: some View {
        VStack {
            Text(value)
            Text("\(counter)")
            Button {
                counter += 1
            } label: {
                Text("increment")
            }
            .buttonStyle(.bordered)
        }.task {
            value = await service.fetchResult()
        }
    }
}

struct IncrementView_Previews: PreviewProvider {
    static var previews: some View {
        IncrementView()
    }
}
