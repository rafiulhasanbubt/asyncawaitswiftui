//
//  Service.swift
//  AsyncAwaitSwiftUI
//
//  Created by rafiul hasan on 13/12/21.
//

import Foundation

class Service {
    func fetchResult() async -> String {
        await sleep(seconds: 5)
        return "Result"
    }
    
    private func sleep(seconds: Int) async {
        try? await Task.sleep(nanoseconds: UInt64(seconds * 1000000000))
    }
}
